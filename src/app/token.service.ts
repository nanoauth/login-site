import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class TokenService {

    token: any;

    load() {
        if (location.hash.startsWith('#id_token=')) {
            this.token = location.hash.split('=')[1];
        }
    }

    getToken() {
        return this.token;
    }
}