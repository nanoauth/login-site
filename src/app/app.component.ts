import { Component, OnInit } from '@angular/core';
import { TokenService } from './token.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  welcomeMessage = 'You are logged in as';
  user = '';
  loggedIn = false;
  address = '';

  constructor(private service: TokenService) {}

  ngOnInit() {
    this.token();
  }

  token() {
    const token = this.service.getToken();
    if (token) {
      const tokenBody = token.split('.')[1];
      this.logInAs('Respected NanoAuth User');
      this.address = JSON.parse(atob(tokenBody)).address;
    } else {
      this.logOut();
    }
    
  }

  onLogoutClick() {
    this.logOut();
  }

  logInAs(name: string) {
    this.user = name;
    this.loggedIn = true;
  }

  logOut() {
    this.user = '';
    this.loggedIn = false;
    location.hash = '';
    this.address = '';
  }

  // example token
  // #id_token=eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjVvRmFxay1JR1BKWTZMbVp0azh0WGdOWF9hWHZTcTF2aGRoTjhtSlhwd3cifQ.eyJzdWIiOiI2YjE0NzcyYi1hM2Q5LTQ1YTAtOGNhMS1iOGIyZWVjNjI4NDIiLCJhdXRoX3RpbWUiOjE1OTEzMDg2NDAsIm5vbmNlIjoiZm9vYmFyIiwiYXVkIjoiZm9vIiwiZXhwIjoxNTkxMzEyMjQwLCJpYXQiOjE1OTEzMDg2NDAsImlzcyI6Imh0dHBzOi8vbmFub2F1dGguYXp1cmV3ZWJzaXRlcy5uZXQifQ.r5Q-O6o-OmuhWg4STpcCNWngMYZla6XaA6OWhk99M9rRyT50ScN9sQcpI_MrxB1giLDw-K_yTcl30TpFifSSce4uU3ogn9-jV80TBTzrRS-x8o_P_6z78Nm8TrxeWR30g6v-aOOCr3_xu-UUq7zlK-nk6ObEIC6y75mMDJ9f-cjAhaccFaPpv3Iq736ZzythKXN8Vq1UYg-z1WJd9pSJSW8yeyb5_5ku_Cw1k46-6kJmzmPLcoBUQT0EQYePIMvMSk3ZbFqU8xbqtEe_UCyrrklF_Mm6w6ZZOEzUr4Rx9KdYTKtFqrNe2ZOXg_VNslxUBdRKPpEDbWoFgwLn7jOyhA

}